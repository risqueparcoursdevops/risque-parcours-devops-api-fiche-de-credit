FROM openjdk:8-alpine
ADD target/rpd-api-fiche-credit-0.0.1-SNAPSHOT.jar rpd-api-fiche-credit.jar
ENTRYPOINT ["java", "-jar", "rpd-api-fiche-credit.jar"]
EXPOSE 8082