package com.edgenda.bnc.rpd.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.edgenda.bnc.rpd.model.FicheCredit;

@Repository
public interface FicheCreditRepository extends JpaRepository<FicheCredit, Long> {

    Optional<FicheCredit> findById(Long id);
    Optional<FicheCredit> findByNoFicheCentralClient(String noFicheCentralClient);

}
