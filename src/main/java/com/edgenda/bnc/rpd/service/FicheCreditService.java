package com.edgenda.bnc.rpd.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.edgenda.bnc.rpd.model.FicheCredit;
import com.edgenda.bnc.rpd.repository.FicheCreditRepository;
import com.edgenda.bnc.rpd.service.exception.UnknownFicheCreditException;

@Service
@Transactional
public class FicheCreditService {

    private final FicheCreditRepository ficheCreditRepository;

    @Autowired
    public FicheCreditService(FicheCreditRepository ficheCreditRepository) {
        this.ficheCreditRepository = ficheCreditRepository;
    }
    
    public FicheCredit getFicheCreditByNoFicheCentralClient(String noFicheCentralClient) {
        Assert.notNull(noFicheCentralClient, "FicheCredit noFicheCentralClient cannot be null");
        Optional<FicheCredit> ficheCredit = ficheCreditRepository.findByNoFicheCentralClient(noFicheCentralClient);
        if(!ficheCredit.isPresent()) {
        	throw new UnknownFicheCreditException(noFicheCentralClient); 
        }
        return ficheCredit.orElse(null);
    }

    public List<FicheCredit> getFicheCredits() {
        return ficheCreditRepository.findAll();
    }
}
