package com.edgenda.bnc.rpd.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnknownFicheCreditException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknownFicheCreditException(Long id) {
        super("Unknown FicheCredit with ID=" + id);
    }

	public UnknownFicheCreditException(String noFicheCredit) {
        super("Unknown FicheCredit with noFicheCredit=" + noFicheCredit); 
    }

}
