package com.edgenda.bnc.rpd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

@Entity(name = "FICHECREDIT")
public class FicheCredit {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column(name="NOFICHECENTRALCLIENT")
    private String noFicheCentralClient;
    
    @NotEmpty
    @Column(name="NOFICHECREDIT")
    private String noFicheCredit;

    @NotEmpty
    private Integer score;

    public FicheCredit() {
    }

    public FicheCredit(Long id, String noFicheCentralClient, String noFicheCredit, Integer score) {
        this.id = id;
        this.noFicheCentralClient = noFicheCentralClient;
        this.noFicheCredit = noFicheCredit;
        this.score = score;
    }

    @PersistenceConstructor
    public FicheCredit(String noFicheCredit, Integer score) {
        this.noFicheCredit = noFicheCredit;
        this.score = score;
    }

	public Long getId() {
		return id;
	}

	public String getNoFicheCentralClient() {
		return noFicheCentralClient;
	}
	
	public String getNoFicheCredit() {
		return noFicheCredit;
	}

	public Integer getScore() {
		return score;
	}

}
