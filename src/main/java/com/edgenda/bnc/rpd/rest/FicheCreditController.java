package com.edgenda.bnc.rpd.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.edgenda.bnc.rpd.model.FicheCredit;
import com.edgenda.bnc.rpd.service.FicheCreditService;

@RestController
@RequestMapping(path = "/ficheCredits")
public class FicheCreditController {

    private final FicheCreditService ficheCreditService;

    @Autowired
    public FicheCreditController(FicheCreditService ficheCreditService) {
        this.ficheCreditService = ficheCreditService;
    }

    @RequestMapping(path = "/{noFicheCentralClient}", method = RequestMethod.GET)
    public FicheCredit getFicheCentralClient(@PathVariable String noFicheCentralClient) {
    	return ficheCreditService.getFicheCreditByNoFicheCentralClient(noFicheCentralClient.toUpperCase());
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<FicheCredit> getAllFicheCredits() {
   	
    	/*RestTemplate restTemplate = new RestTemplate();
    	HttpHeaders headers = new HttpHeaders();
    	headers.set("Host", "rpd-api-fiche-credit-host.com");
    	headers.set("Host", "rpd-api-decision-host.com");
    	RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, new URI("http://rpd-kong:8000"));
    	System.out.println(restTemplate.exchange(requestEntity, String.class).getBody());*/
    	
    	return ficheCreditService.getFicheCredits();
    }
}
