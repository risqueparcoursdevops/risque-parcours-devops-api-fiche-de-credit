package com.edgenda.bnc.rpd.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/heartbeat")
public class HeartbeatController {

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Void> heartbeat() {
   		return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
