package com.edgenda.bnc.rpd.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.edgenda.bnc.rpd.model.FicheCredit;
import com.edgenda.bnc.rpd.repository.FicheCreditRepository;
import com.edgenda.bnc.rpd.service.exception.UnknownFicheCreditException;

@RunWith(MockitoJUnitRunner.class)
public class FicheCreditRepositoryTest {

	private FicheCreditService ficheCreditService;
	
	@Mock
	private FicheCreditRepository ficheCreditRepositoryMock;
	
	@Before
	public void setUp() {
		this.ficheCreditService = new FicheCreditService(ficheCreditRepositoryMock);
	}
	
	@Test
	public void testGetFicheCreditByNoFicheCredit() {
		FicheCredit expectedFicheCredit = new FicheCredit(1l, "FCC-001", "ABCDE", 1000);
		Mockito.when(this.ficheCreditRepositoryMock.findByNoFicheCentralClient(Mockito.anyString())).thenReturn(Optional.of(expectedFicheCredit));
		
		Assert.assertEquals(expectedFicheCredit.getId(), this.ficheCreditService.getFicheCreditByNoFicheCentralClient(new String()).getId());
		Assert.assertEquals(expectedFicheCredit.getNoFicheCentralClient(), this.ficheCreditService.getFicheCreditByNoFicheCentralClient(new String()).getNoFicheCentralClient());
		Assert.assertEquals(expectedFicheCredit.getNoFicheCredit(), this.ficheCreditService.getFicheCreditByNoFicheCentralClient(new String()).getNoFicheCredit());
		Assert.assertEquals(expectedFicheCredit.getScore(), this.ficheCreditService.getFicheCreditByNoFicheCentralClient(new String()).getScore());
	}
	
	@Test(expected=UnknownFicheCreditException.class)
	public void testGetFicheCreditByNoFicheCentralClientThrowException() {
		Mockito.when(this.ficheCreditRepositoryMock.findByNoFicheCentralClient(Mockito.anyString())).thenReturn(Optional.empty());
		this.ficheCreditService.getFicheCreditByNoFicheCentralClient(new String());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetFicheCreditByNoFicheCentralClientNullThrowException() {
		this.ficheCreditService.getFicheCreditByNoFicheCentralClient(null);
	}
	
	@Test
	public void testGetFicheCredits() {
		List<FicheCredit> expectedFicheCredits = Arrays.asList(new FicheCredit("ABCDE", 1000), new FicheCredit());
		Mockito.when(this.ficheCreditRepositoryMock.findAll()).thenReturn(expectedFicheCredits);
		
		Assert.assertEquals(expectedFicheCredits, this.ficheCreditService.getFicheCredits());
	}

}
