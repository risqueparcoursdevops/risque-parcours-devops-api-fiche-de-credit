package com.edgenda.bnc.rpd.service.exception;

import org.junit.Assert;
import org.junit.Test;


public class UnknownFicheCreditExceptionTest {

	@Test
	public void testConstructorId() {
		Long id = Long.MAX_VALUE;
		
		UnknownFicheCreditException unknownFicheCreditException = new UnknownFicheCreditException(id);
		Assert.assertNotNull(unknownFicheCreditException);
		Assert.assertEquals("Unknown FicheCredit with ID=" + id.toString(), unknownFicheCreditException.getMessage());
	}
	
	@Test
	public void testConstructorNoFicheCredit() {
		String noFicheCredit = "ABCDEF";
		
		UnknownFicheCreditException unknownFicheCreditException = new UnknownFicheCreditException(noFicheCredit);
		Assert.assertNotNull(unknownFicheCreditException);
		Assert.assertEquals("Unknown FicheCredit with noFicheCredit=" + noFicheCredit, unknownFicheCreditException.getMessage());
	}

}
