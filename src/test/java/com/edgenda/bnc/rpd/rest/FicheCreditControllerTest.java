package com.edgenda.bnc.rpd.rest;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.edgenda.bnc.rpd.model.FicheCredit;
import com.edgenda.bnc.rpd.service.FicheCreditService;

@RunWith(MockitoJUnitRunner.class)
public class FicheCreditControllerTest {

	private FicheCreditController ficheCreditController;
	
	@Mock
	private FicheCreditService ficheCreditServiceMock;
	
	@Before
	public void setUp() {
		this.ficheCreditController = new FicheCreditController(this.ficheCreditServiceMock);
	}
	
	@Test
	public void testGetFicheCredit() {
		FicheCredit expectedFicheCredit = new FicheCredit(1l, "FCC-001", "ABCDE", 1000);
		Mockito.when(this.ficheCreditServiceMock.getFicheCreditByNoFicheCentralClient(Mockito.anyString())).thenReturn(expectedFicheCredit);
		
		FicheCredit response = this.ficheCreditController.getFicheCentralClient(new String());
		Assert.assertEquals(expectedFicheCredit, response);
	}
	
	@Test
	public void testGetAllFicheCredits() {
		List<FicheCredit> expectedFicheCredits = Arrays.asList(new FicheCredit(1l, "FCC-001", "ABCDE", 1000));
		Mockito.when(this.ficheCreditServiceMock.getFicheCredits()).thenReturn(expectedFicheCredits);
		
		List<FicheCredit> response = this.ficheCreditController.getAllFicheCredits();
		Assert.assertEquals(expectedFicheCredits, response);
	}
}
