package com.edgenda.bnc.rpd.rest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class HeartbeatControllerTest {

	private HeartbeatController heartbeatController;
	
	@Before
	public void setUp() {
		this.heartbeatController = new HeartbeatController();
	}
	
	@Test
	public void testHeartbeat() {
		ResponseEntity<Void> response = this.heartbeatController.heartbeat();
		Assert.assertEquals(200, response.getStatusCodeValue());
	}
	
}
